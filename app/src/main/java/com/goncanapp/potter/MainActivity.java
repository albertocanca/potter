package com.goncanapp.potter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.percent.PercentRelativeLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.goncanapp.potter.Utils.Answer;
import com.goncanapp.potter.Utils.AnswersBD;
import com.goncanapp.potter.Utils.Constants;
import com.goncanapp.potter.Utils.Files;
import com.goncanapp.potter.Utils.IdentificatorAsyncTask;
import com.goncanapp.potter.Utils.AnswersAsyncTask;
import com.goncanapp.potter.Utils.UpdateUserAsyncTask;

import org.json.JSONException;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.ExecutionException;

public class MainActivity extends AppCompatActivity {

    public static Context contexto;
    public static ArrayList<Answer> answers;
    Answer answer;
    TextView answer_txt;
    Button response1, response2, response3, response4;
    FloatingActionButton nextBtn, infoBtn;
    PercentRelativeLayout empty;

    ArrayList<Button> buttons;

    // Progress Dialog
    ProgressDialog pDialog;

    public static String identificator;
    public static String idAnswer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        contexto = this;
        answers = new ArrayList<>();

        answer_txt = (TextView) findViewById(R.id.answerTxt);

        response1 = (Button) findViewById(R.id.response1Btn);
        response2 = (Button) findViewById(R.id.response2Btn);
        response3 = (Button) findViewById(R.id.response3Btn);
        response4 = (Button) findViewById(R.id.response4Btn);

        buttons = new ArrayList<>();
        buttons.add(response1);
        buttons.add(response2);
        buttons.add(response3);
        buttons.add(response4);

        nextBtn = (FloatingActionButton) findViewById(R.id.nextBtn);
        infoBtn = (FloatingActionButton) findViewById(R.id.infoBtn);

        //Cursor cursor = mitosBD.obtenerMitos();
        try {
            //Get identificator
            identificator = getIdentificator();

            //Open dialog
            pDialog = new ProgressDialog(MainActivity.this);
            pDialog.setMessage("Cargando preguntas. Por favor espere...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();

            //Ger mitos from BD
            AnswersAsyncTask answersAsyncTask = new AnswersAsyncTask();
            answersAsyncTask.execute().get();
            answers = answersAsyncTask.getAnswers();

            //Close dialog
            pDialog.dismiss();

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        reload();

        response1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getResult("1");
            }
        });
        response2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getResult("2");
            }
        });
        response3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getResult("3");
            }
        });
        response4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getResult("4");
            }
        });

        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                reload();
            }
        });

        infoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openAlertInfo();
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.acerca_de:
                Intent i= new Intent(this,AnswerActivity.class);
                startActivityForResult(i, 1);
                overridePendingTransition(R.animator.zoom_forward_in, R.animator.zoom_forward_out);
                return true;
            case R.id.listview_answers:
                Intent i2= new Intent(this,AnswersList.class);
                startActivityForResult(i2, 1);
                overridePendingTransition(R.animator.zoom_forward_in, R.animator.zoom_forward_out);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    private void removeMito() {
        answers.remove(answer);
    }

    private void reload() {
        if (answers.isEmpty()) {
            answer = new Answer("0", "Próximamente habrá mas preguntas", "", "","" ,"", "","", "");
            response1.setVisibility(View.INVISIBLE);
            response2.setVisibility(View.INVISIBLE);
            response3.setVisibility(View.INVISIBLE);
            response4.setVisibility(View.INVISIBLE);

            empty = (PercentRelativeLayout) findViewById(R.id.empty);
            empty.setVisibility(View.VISIBLE);
        } else {
            answer = anyMito(answers);
            idAnswer = answer.getId();
        }

        response1.setBackgroundDrawable(getResources().getDrawable(R.drawable.layout_rounded));
        response1.setEnabled(true);

        response2.setBackgroundDrawable(getResources().getDrawable(R.drawable.layout_rounded));
        response2.setEnabled(true);

        response3.setBackgroundDrawable(getResources().getDrawable(R.drawable.layout_rounded));
        response3.setEnabled(true);

        response4.setBackgroundDrawable(getResources().getDrawable(R.drawable.layout_rounded));
        response4.setEnabled(true);

        nextBtn.setVisibility(View.INVISIBLE);
        infoBtn.setVisibility(View.INVISIBLE);

        answer_txt.setText(answer.getAnswer());
        response1.setText(answer.getResponse1());
        response2.setText(answer.getResponse2());
        response3.setText(answer.getResponse3());
        response4.setText(answer.getResponse4());
    }

    private void disabledAndEnabledButtons() {
        nextBtn.setVisibility(View.VISIBLE);
        infoBtn.setVisibility(View.VISIBLE);
        response1.setEnabled(false);
        response2.setEnabled(false);
        response3.setEnabled(false);
        response4.setEnabled(false);
    }

    public Answer anyMito(ArrayList<Answer> mitos) {
        Random r = new Random();
        int index = r.nextInt(mitos.size());
        return mitos.get(index);
    }

    public void getResult(String value){
        if (answer.getResponseSuccess().equals(value)) {
            //Acertado
            for(int i=1; i < buttons.size(); i++){
                int buttonId = i-1;
                if(String.valueOf(i).equals(value)){
                    buttons.get(buttonId).setBackgroundDrawable(getResources().getDrawable(R.drawable.layout_rounded_exit));
                }
            }
        } else {
            //Fallado
            for(int i=1; i < buttons.size()+1; i++){
                int buttonId = i-1;
                if(String.valueOf(i).equals(value)){
                    buttons.get(buttonId).setBackgroundDrawable(getResources().getDrawable(R.drawable.layout_rounded_fail));
                }
                if(String.valueOf(i).equals(answer.getResponseSuccess())){
                    buttons.get(buttonId).setBackgroundDrawable(getResources().getDrawable(R.drawable.layout_rounded_exit));
                }
            }
        }

        disabledAndEnabledButtons();
        updateUserBD();
    }

    public void openAlertInfo(){
        AlertDialog.Builder dialog = new AlertDialog.Builder(contexto);
        dialog.setTitle(answer.getTitleInformation())
                .setIcon(R.drawable.info)
                .setMessage(answer.getInformation())
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                    }
                }).show();
    }

    public static String getIdentificator() throws JSONException, ExecutionException, InterruptedException {
        Files files = new Files(contexto);
        String data = files.read(Constants.TAG_FILENAME_IDENTIFICATOR);
        String identificator = "";
        if(!data.equals("")){
            identificator = files.deserialize(Constants.TAG_KEY_IDENTIFICATOR, data);
        }
        if(identificator.equals("")){
            IdentificatorAsyncTask identificatorAsyncTask = new IdentificatorAsyncTask();
            identificatorAsyncTask.execute().get();
            identificator = identificatorAsyncTask.getIdentificator();
            Log.i("Identificator: ", identificator);
            data = files.serialize(Constants.TAG_KEY_IDENTIFICATOR, identificator);
            files.write(Constants.TAG_FILENAME_IDENTIFICATOR, data);
        }

        return identificator;
    }

    public void updateUserBD(){
        try {
            UpdateUserAsyncTask updateUserAsyncTask = new UpdateUserAsyncTask();
            updateUserAsyncTask.execute().get();

            saveAnswersJson();
            removeMito();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    private void saveAnswersJson() {
        AnswersBD answersBD = new AnswersBD(contexto);
        answersBD.saveAnswers(answer);
    }
}
