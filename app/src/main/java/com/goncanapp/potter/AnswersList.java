package com.goncanapp.potter;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import com.goncanapp.potter.Adapter.ListViewAdapter;
import com.goncanapp.potter.Utils.Answer;
import com.goncanapp.potter.Utils.AnswerLite;
import com.goncanapp.potter.Utils.AnswersBD;

import java.util.ArrayList;

/**
 * Created by Alberto on 23/07/2016.
 */
public class AnswersList extends AppCompatActivity {

    ListView lista;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.listview_answers);

        lista = (ListView) findViewById(R.id.listViewAnswers);

        ArrayList<AnswerLite> answers = new ArrayList<AnswerLite>();
        AnswersBD answersBD = new AnswersBD(this);
        Cursor cursor = answersBD.getAnswers();
        while (cursor.moveToNext()) {
            AnswerLite answerLite = new AnswerLite(cursor.getString(0), cursor.getString(1), cursor.getString(2),cursor.getString(3));
            answers.add(answerLite);
        }

        lista.setAdapter(new ListViewAdapter(this, answers));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.acerca_de:
                Intent i= new Intent(this,AcercaDe.class);
                startActivityForResult(i, 1);
                overridePendingTransition(R.animator.zoom_forward_in, R.animator.zoom_forward_out);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


}
