package com.goncanapp.potter;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.goncanapp.potter.Utils.Constants;
import com.goncanapp.potter.Utils.Files;

import org.json.JSONException;

/**
 * Created by Alberto on 15/07/2016.
 */
public class AcercaDe extends AppCompatActivity {

    Context contexto;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acerca_de);

        contexto  = this;

        TextView identificatorText = (TextView) findViewById(R.id.identificatorText);
        MainActivity mainActivity = new MainActivity();
        identificatorText.setText(getIdentificatorFromJSON());
    }

    public String getIdentificatorFromJSON (){
        Files files = new Files(contexto);
        String data = files.read(Constants.TAG_FILENAME_IDENTIFICATOR);
        String identificator = "";
        if(!data.equals("")){
            try {
                identificator = files.deserialize(Constants.TAG_KEY_IDENTIFICATOR, data);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return identificator;
    }
}
