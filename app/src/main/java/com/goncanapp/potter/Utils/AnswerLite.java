package com.goncanapp.potter.Utils;

/**
 * Created by Alberto on 23/07/2016.
 */
public class AnswerLite {

    String id;
    String answer;
    String titleInformation;
    String information;

    public AnswerLite(String id, String answer, String titleInformation, String information) {
        this.id = id;
        this.answer = answer;
        this.titleInformation = titleInformation;
        this.information = information;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getInformation() {
        return information;
    }

    public void setInformation(String information) {
        this.information = information;
    }

    public String getTitleInformation() {
        return titleInformation;
    }

    public void setTitleInformation(String titleInformation) {
        this.titleInformation = titleInformation;
    }
}

