package com.goncanapp.potter.Utils;

import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Alberto on 13/07/2016.
 */
public class IdentificatorAsyncTask extends AsyncTask<String, String, String> {

    // Creating JSON Parser object
    JSONParser jParser = new JSONParser();

    // url to get all products list
    String url_get_identificator = Constants.TAG_URL_IDENTIFICATOR;

    String identificator = "";

    @Override
    protected void onPreExecute() {
        identificator = "";
        super.onPreExecute();
    }

    protected String doInBackground(String... args) {
        // Building Parameters
        ArrayList params = new ArrayList();
        // getting JSON string from URL
        JSONObject json = jParser.makeHttpRequest(url_get_identificator, "GET", params);

        try {
            // Checking for SUCCESS TAG
            int success = json.getInt("success");

            if (success == 1) {
                identificator = json.getString("identificator");

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    protected void onPostExecute(String file_url) {

    }

    public String getIdentificator(){
        return identificator;
    }
}
