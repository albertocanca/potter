package com.goncanapp.potter.Utils;

/**
 * Created by Alberto on 23/07/2016.
 */
public class Answer {

    String id;
    String answer;
    String response1;
    String response2;
    String response3;
    String response4;
    String responseSuccess;
    String titleInformation;
    String information;

    public Answer(String id, String answer, String response1, String response2, String response3, String response4, String responseSuccess, String titleInformation, String information) {
        this.id = id;
        this.answer = answer;
        this.response1 = response1;
        this.response2 = response2;
        this.response3 = response3;
        this.response4 = response4;

        this.responseSuccess = responseSuccess;
        this.titleInformation = titleInformation;
        this.information = information;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getResponse1() {
        return response1;
    }

    public void setResponse1(String response1) {
        this.response1 = response1;
    }

    public String getResponse2() {
        return response2;
    }

    public void setResponse2(String response2) {
        this.response2 = response2;
    }

    public String getResponse3() {
        return response3;
    }

    public void setResponse3(String response3) {
        this.response3 = response3;
    }

    public String getResponse4() {
        return response4;
    }

    public void setResponse4(String response4) {
        this.response4 = response4;
    }

    public String getResponseSuccess() {
        return responseSuccess;
    }

    public void setResponseSuccess(String responseSuccess) {
        this.responseSuccess = responseSuccess;
    }

    public String getInformation() {
        return information;
    }

    public void setInformation(String information) {
        this.information = information;
    }

    public String getTitleInformation() {
        return titleInformation;
    }

    public void setTitleInformation(String titleInformation) {
        this.titleInformation = titleInformation;
    }
}

