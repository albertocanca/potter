package com.goncanapp.potter.Utils;

/**
 * Created by Alberto on 15/07/2016.
 */
public class Constants {

    //URL
    public static String TAG_URL_ANSWERS_BY_USER = "http://mitorealidad.esy.es/hp_getAnswersByUser.php";
    public static String TAG_URL_IDENTIFICATOR = "http://mitorealidad.esy.es/hp_getIdentificator.php";
    public static String TAG_URL_UPDATE_USER = "http://mitorealidad.esy.es/hp_updateAnswersByUser.php";

    //Files
    public static String TAG_FILENAME_IDENTIFICATOR = "hp_identificador.json";
    public static String TAG_FILENAME_ANSWERS = "hp_answers.json";

    //KEY
    public static String TAG_KEY_IDENTIFICATOR = "hp_identificator";
    public static String TAG_KEY_ANSWERS = "answers";
}
