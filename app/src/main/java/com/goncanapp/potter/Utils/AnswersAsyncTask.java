package com.goncanapp.potter.Utils;

import android.os.AsyncTask;

import com.goncanapp.potter.MainActivity;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Alberto on 13/07/2016.
 */
public class AnswersAsyncTask extends AsyncTask<String, String, String> {

    // Creating JSON Parser object
    JSONParser jParser = new JSONParser();

    // url to get all products list
    String url_all_answers = Constants.TAG_URL_ANSWERS_BY_USER;

    // products JSONArray
    JSONArray products = null;

    ArrayList<Answer> answersAsyncTask;

    @Override
    protected void onPreExecute() {
        answersAsyncTask =  new ArrayList<>();
        super.onPreExecute();
    }

    /**
     * obteniendo todos los productos
     * */
    protected String doInBackground(String... args) {
        // Building Parameters
        ArrayList params = new ArrayList();
        params.add(new BasicNameValuePair("identificator", MainActivity.identificator));
        // getting JSON string from URL
        JSONObject json = jParser.makeHttpRequest(url_all_answers, "POST", params);

        try {
            // Checking for SUCCESS TAG
            int success = json.getInt("success");

            if (success == 1) {
                // products found
                // Getting Array of Products
                products = json.getJSONArray(Constants.TAG_KEY_ANSWERS);

                // looping through All Products
                for (int i = 0; i < products.length(); i++) {
                    JSONObject c = products.getJSONObject(i);

                    // Storing each json item in variable
                    String id = c.getString("id");
                    String answer = c.getString("answer");
                    String response1 = c.getString("response1");
                    String response2 = c.getString("response2");
                    String response3 = c.getString("response3");
                    String response4 = c.getString("response4");
                    String response_success = c.getString("response_success");
                    String title_information = c.getString("title_information");
                    String information = c.getString("information");

                    Answer object = new Answer(id, answer, response1, response2, response3, response4, response_success, title_information, information );
                    answersAsyncTask.add(object);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    protected void onPostExecute(String file_url) {

    }

    public ArrayList<Answer> getAnswers(){
        return answersAsyncTask;
    }
}
