package com.goncanapp.potter.Utils;

import android.os.AsyncTask;

import com.goncanapp.potter.MainActivity;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

/**
 * Created by Alberto on 13/07/2016.
 */
public class UpdateUserAsyncTask extends AsyncTask<String, String, String> {

    // Creating JSON Parser object
    JSONParser jParser = new JSONParser();

    // url to get all products list
    String url_update_user = Constants.TAG_URL_UPDATE_USER;

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    protected String doInBackground(String... args) {
        // Building Parameters
        ArrayList params = new ArrayList();
        try {
            params.add(new BasicNameValuePair("identificator", MainActivity.getIdentificator()));
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        params.add(new BasicNameValuePair("answer", MainActivity.idAnswer));

        // getting JSON string from URL
        JSONObject json = jParser.makeHttpRequestUpdate(url_update_user, "POST", params);

        return null;
    }

    protected void onPostExecute(String file_url) {

    }
}
