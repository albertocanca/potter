package com.goncanapp.potter.Utils;

/**
 * Created by Alberto on 03/07/2016.
 */
public class Mito {

    String id;
    String answer;
    String description;
    String information;
    String response;
    String tweet;

    public Mito(String id, String answer,String response, String information, String tweet) {
        this.id = id;
        this.answer = answer;
        this.information = information;
        this.response = response;
        this.tweet = tweet;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAswer(String answer) {
        this.answer = answer;
    }

    public String getInformation() {
        return information;
    }

    public void setInformation(String information) {
        this.information = information;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getTweet() {
        return tweet;
    }

    public void setTweet(String tweet) {
        this.tweet = tweet;
    }
}
