package com.goncanapp.potter.Utils;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Alberto on 07/09/2015.
 */
public class AnswersBD extends SQLiteOpenHelper {

    public AnswersBD(Context context) {
        super(context, "Answers", null, 2);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //Crear tabla VIAJES
        db.execSQL("CREATE TABLE answers( id INTEGER PRIMARY KEY AUTOINCREMENT, answer TEXT, titleInformation TEXT, information TEXT)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public Cursor getAnswers() {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM answers", null);
        return cursor;
    }

    public void saveAnswers(Answer answer) {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("INSERT INTO answers VALUES (null, '" + answer.getAnswer() + "', '" + answer.getTitleInformation() + "' , '"  + answer.getInformation() + "')");
        db.close();
    }
}
