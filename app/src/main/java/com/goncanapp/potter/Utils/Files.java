package com.goncanapp.potter.Utils;

import android.content.Context;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

/**
 * Created by Alberto on 15/07/2016.
 */
public class Files {

    Context context;

    public Files(Context context){
        this.context = context;
    }

    public Files() {

    }


    public boolean existenciaFichero( String nombre){

        File file = new File(context.getExternalFilesDir(null),nombre);
        if (file.exists()){
            return true;
        }else{
            return false;
        }
    }

    public String serialize (String key, String identificator) {
        try {
            JSONObject jsonObj = new JSONObject();
            jsonObj.put(key, identificator);
            return jsonObj.toString();
        }
        catch(JSONException ex) {
            ex.printStackTrace();
        }

        return null;
    }

    public String deserialize(String key, String data) throws JSONException {
        JSONObject jObj = new JSONObject(data);
        Object object = jObj.get(key);
        return object.toString();
    }

    public void empty (String fileName){
        try{
            File file = new File(context.getExternalFilesDir(null),fileName);
            OutputStreamWriter fout = new OutputStreamWriter(new FileOutputStream(file));
            fout.write("");
            fout.close();
        }
        catch (Exception ex){
            Log.e("Ficheros", "Error al vaciar el fichero");
        }
    }

    public void write (String fileName, String data){

        empty(fileName);

        try{
            File file = new File(context.getExternalFilesDir(null),fileName);
            OutputStreamWriter fout = new OutputStreamWriter(new FileOutputStream(file));
            fout.write(data);
            fout.close();
        }
        catch (Exception ex){
            Log.e("Ficheros", "Error al escribir fichero");
        }
    }

    public String read (String filename){

        String res = "";
        if(existenciaFichero(filename)){
            try {
                File file = new File(context.getExternalFilesDir(null),filename);
                BufferedReader fin = new BufferedReader(new InputStreamReader( new FileInputStream(file)));
                res = fin.readLine();
                fin.close();

            } catch (Exception ex){
                Log.e("Ficheros", "Error al leer fichero");
                ex.printStackTrace();
            }
        }

        return res;
    }

}
