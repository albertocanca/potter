package com.goncanapp.potter.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.goncanapp.potter.R;
import com.goncanapp.potter.Utils.AnswerLite;

import java.util.ArrayList;

/**
 * Created by Alberto on 23/07/2016.
 */
public class ListViewAdapter extends BaseAdapter {

    // Declare Variables
    Context context;
    LayoutInflater inflater;
    ArrayList<AnswerLite> answers;

    public ListViewAdapter(Context context, ArrayList<AnswerLite> answers) {
        this.context = context;
        this.answers = answers;
    }

    @Override
    public int getCount() {
        return answers.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        TextView txtTitle;
        TextView txtContenido;

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View itemView = inflater.inflate(R.layout.list_row, parent, false);

        txtTitle = (TextView) itemView.findViewById(R.id.titulo);
        txtContenido = (TextView) itemView.findViewById(R.id.description);

        txtTitle.setText(answers.get(position).getTitleInformation());
        txtContenido.setText(answers.get(position).getInformation());

        return itemView;
    }
}